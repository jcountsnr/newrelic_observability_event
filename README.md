# Bitbucket Pipelines Pipe: New Relic Pipe

This pipe is used to send an event to [New Relic](https://newrelic.com/). The events will be sent to our [Event API](https://docs.newrelic.com/docs/data-apis/ingest-apis/event-api/introduction-event-api/), and can be queried and visualized in the New Relic UI. This integration is available in the New Relic I/O marketplace: [Bitbucket quickstart](https://newrelic.com/instant-observability/bitbucket/fdc99da8-0061-4c5f-bd90-ef67a8b2419f?utm_source=medium&utm_medium=press&utm_campaign=global-fy22-q4-io-partner)

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: jcountsnr/newrelic_observability_event:0.0.7
    variables:
      INGEST_KEY: "<string>"
      EVENTTYPE: "<string>"  #Optional
      MESSAGE: "<string>"    #Optional
      PRIORITY: "<string>"   #Optional
      EU_ENDPOINT: "<boolean>" #Optional

```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| INGEST_KEY (*)        | Your New Relic Ingest key |
| EVENTTYPE             | The name of your Event in New Relic |
| MESSAGE               | The content of your event |
| PRIORITY              | Give a priority for the alert sent to New Relic |
| EU_ENDPOINT           | Boolean, note whether or not your New Relic instance is EU - default value is `false` |
| CUSTOM                | New Relic supports custom events, so you can add any custom value to this pipe |

_(*) = required variable._

## Prerequisites

## Examples

Basic example:

```yaml
script:
  - pipe: jcountsnr/newrelic_observability_event:0.0.7
    variables:
      INGEST_KEY: "1234"
```

Advanced example:

```yaml
script:
  - pipe: jcountsnr/newrelic_observability_event:0.0.7
    variables:
      INGEST_KEY: "1234"
      EVENTTYPE: "pipeline_alert"
      MESSAGE: "example message"
      PRIORITY: "normal"
      EU-ENDPOINT: "true"
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by jcounts@newrelic.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce

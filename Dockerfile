FROM alpine:3.9

RUN apk --no-cache add \
    bash=4.4.19-r1 \
    curl=7.64.0-r5 \
    jq=1.6-r0

RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh

COPY pipe /
COPY LICENSE.txt README.md pipe.yml /

ENTRYPOINT ["/pipe.sh"]

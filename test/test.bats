#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="jcounts002/newrelic_observability_event"}
  echo "Building image..."
  docker build -t ${DOCKER_IMAGE}:test .
}

teardown() {
  rm -rf $(pwd)/tmp/pipe-*.txt
}

@test "attempting event push to New Relic" {
    run docker run \
        -e INGEST_KEY=$INGEST_KEY \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
    ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    
}


# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.0.7

- patch: fix eu issue

## 0.0.6

- patch: Remove bb curl and update response

## 0.0.5

- patch: updated logo

## 0.0.4

- patch: fix readme issue and update tag

## 0.0.3

- patch: Internal maintenance: fix some issues

## 0.0.2

- patch: fixed release# on readme

## 0.0.1

- patch: fix various issues

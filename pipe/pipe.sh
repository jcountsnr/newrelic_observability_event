#!/usr/bin/env bash
#
# This pipe is used to send an event to New Relic. https://docs.newrelic.com/docs/data-apis/ingest-apis/event-api/introduction-event-api/
#
# Required parameters:
# INGEST_KEY
#
# Optional parameters
# MESSAGE
# PRIORITY
# EU_ENDPOINT


echo "STEP 1:Executing the pipe..."

# Required parameters

YOUR_LICENSE_KEY=${INGEST_KEY:?'Ingest Key variable missing.'}

# Default parameters
EVENTTYPE=${EVENTTYPE:="bitbucket_pipeline"}
MESSAGE=${MESSAGE:="Event sent from Pipeline #${BITBUCKET_BUILD_NUMBER}"}
PRIORITY=${PRIORITY:='normal'}
EXIT_CODE=${EXIT_CODE:=${BITBUCKET_EXIT_CODE}}
REPO_URL=${REPO_URL:=${BITBUCKET_GIT_HTTP_ORIGIN}}
EU_ENDPOINT=${EU_ENDPOINT:=false}

workspace=${BITBUCKET_WORKSPACE}
# Getting Log info
echo "STEP 2:building payload"
payload=$(jq -n \
  --arg EVENTTYPE "${EVENTTYPE}" \
  --arg MESSAGE "${MESSAGE}" \
  --arg PRIORITY "${PRIORITY}" \
  --arg EXIT_CODE "${EXIT_CODE}" \
  --arg REPO_URL "${REPO_URL}" \
  '{
    "eventType": $EVENTTYPE,
    "message": $MESSAGE,
    "priority": $PRIORITY,
    "exitCode": $EXIT_CODE,
    "repoUrl": $REPO_URL,
}')
echo "Payload: $payload"

endpoint=`$EU_ENDPOINT && echo "insights-collector.eu01.nr-data.net" || echo "insights-collector.newrelic.com"`

echo "STEP 3:making final curl request to NR"

curl -X POST -H "Content-Type: application/json" -H "Api-Key: $INGEST_KEY" -d "$payload" -o response.txt -w "%{http_code}" --silent -i "https://$endpoint/v1/accounts/event" && echo ":  <--API Response code. Check response.txt for full response." || echo "Something failed. New Relic event was not sent."
